package implementation

class MyImplementation {
  def plus(a: Int, b: Int) = {
    println("Using JS specific plus implementation")
    a + b
  }
}
