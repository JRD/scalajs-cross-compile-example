package implementation

class MyImplementation {
  def plus(a: Int, b: Int) = {
    println("Using JVM specific plus implementation")
    a + b
  }
}
