object Main {
  def main(args: Array[String]): Unit = {
    val lib = new MyLibrary
    println(lib.sq(2))
    println(lib.plus(3, 4))
  }
}
