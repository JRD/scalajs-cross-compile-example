# Scala.js cross compile example

This is an example on how to cross compile code to Scala.js and Scala JVM.

To try it out, launch sbt and type:

    sbt> fooJS/run
    sbt> fooJVM/run

More information can be found in the
[Scala.js documentation](http://www.scala-js.org/doc/sbt/cross-building.html).

# Problem when importing in Intellij IDEA

One use case of shared code between JVM and JS is to have most of the code shared between JVM and JS, however, platform
specific implementations could expose the same api while having a different implementation. In such case the _shared_
code module either uses code from the _js_ or the _jvm_ module at compile time. IntellijJ does not yet handle this use
case well. The _shared_ module is unaware of the code in the _jvm_ and/or the _js_ module. Leading to errors like below:

![shared-code-problem.png](https://bitbucket.org/repo/Ayjgax/images/17034315-shared-code-problem.png)

One fix is to manually add the _js_ and _jvm_ module as a dependency to the _shared_ module. This fixes things, however
now Intellij shows the same class twice when importing the class (this is only a minor annoyance, one fix would
obviously be to only depend on either the _js_ or _jvm_ module, but this would largely defeat the purpose of having a
shared module)

![shared-code-problem2.png](https://bitbucket.org/repo/Ayjgax/images/501030879-shared-code-problem2.png)

Since this feels like something near impossible to fix, maybe allow users to chose their poison, when importing a cross
project let the user chose to add dependencies on the _js_ and/or _jvm_ module to the _shared_ module.