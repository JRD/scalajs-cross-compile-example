import implementation.MyImplementation

class MyLibrary {
  val imp = new MyImplementation

  def sq(x: Int): Int = x * x
  def plus(a: Int, b: Int) = imp.plus(a , b)
}
